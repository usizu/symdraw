// Symdraw      by            aks             andrewkenstewart.com
//              contributions andai           andai.tv
//                            marc vinyals

// cc non commercial attribution license

// import default from '../node_modules/chalk/source/index.js';
// console.log(chalk.bgCyan("Chalk Test"));

let version = "v1.2.92"; let lastUpdated = "27 September 2020";
console.log('Symdraw ' + version);

// print into HTML doc
document.getElementById("symdraw-version").innerHTML = version + " - " + lastUpdated;

// General setup stuff
var prevArt = false;
function preload() {
  // prevArt = loadImage('loadimage/symdraw.png');
}

let canvasWidth = 1100; let canvasHeight = 800;
var paintingAreaHeight = canvasHeight;

let uiHeight = 22; let consoleHeight = uiHeight; 
var uiPosition = paintingAreaHeight-consoleHeight;

// UI and Console stuff
let consoleTextSize = 11;
var paletteWidth = 60;
var paletteHeight = uiHeight;
var pickerPreviewWidth  = paletteWidth;
var pickerPreviewHeight = paletteHeight;
var paintColourKeyNum = 1;

var uiLayer, paintLayer, cursorLayer, bgStack;
var uiIsVisible = true;
var ui = {
  top: 2,
  marginV: 2,
  marginH: 2,
  bottom: uiHeight - 2,
  'bgColour': [109, 0, 250, 255], 'logText': [150]
}

// Brush stuff
let brushSize = 9; let paintEraser = false; let radialModeOn = false;

var paintPalette = {
  '1':            [246,220,240, 55], /* Red        */ '6':        [155,250,206, 55], /* Dark Blue  */
  '2':            [ 22,198,250, 55], /* Orange     */ '7':        [185,150,240, 55], /* Indigo     */
  '3':            [ 40,245,255, 55], /* Yellow     */ '8':        [180,120, 80, 55], /* Old Grape  */
  '4':            [ 80,100,239, 55], /* Pale Green */ '0':        [255,  0,255,255], /* Background */
  '5':            [141,200,180, 55], /* Blue       */ 'eraser':   [255,  0,255,155],
  'paintRainbow': [  1,255,255,15],  /* Rainbow!*/    
}

var paintColour  = paintPalette['1'];
var paintCurrent = paintColour;
var activateRainbow = false;

var brushParameters;
var brushesAreOn = true;

var px, py; // prev mouse coordinates
var nx, ny // lerp mouse coordinates
var mirrorX = true; var mirrorY = false;

var colourPickerOn = false;
var hideCursor = false;

// Undo Stuff
var undoCount = 0;
var undoMax = 50;
var redoStack = [];
var canvasClearCheck = false;


// Setup Program
function setup() {
  let cnv = createCanvas(canvasWidth, canvasHeight);
  cnv.parent('p5-symdraw');
  colorMode(HSB, 255);
  pixelDensity(1); // so its the same on all screens ie. Retina

  bgStack = [];
  bgStack[0] = createGraphics(canvasWidth, paintingAreaHeight)
  bgStack[0].colorMode(HSB, 255);
  bgStack[0].background(paintPalette['0']);
  
  bgStack[1] = createGraphics(canvasWidth, paintingAreaHeight)
  bgStack[1].colorMode(HSB, 255);
  if (prevArt) { bgStack[1].image(prevArt, 0, 0); }

  paintLayer = createGraphics(canvasWidth, paintingAreaHeight);
  paintLayer.colorMode(HSB, 255);
  paintLayer.noStroke();
  paintLayer.ellipseMode(CENTER);
  paintLayer.rectMode(CENTER);

  paintStack = [];
  newpaintStackItem(0);

  uiLayer = createGraphics(canvasWidth, consoleHeight);
  uiLayer.colorMode(HSB, 255);
  uiLayer.background(ui['bgColour']);
  uiLayer.noStroke();
  uiLayer.rectMode(CORNER);
  
  cursorLayer = createGraphics(canvasWidth, canvasHeight);
  cursorLayer.colorMode(HSB, 255);
  cursorLayer.noFill();
  cursorLayer.ellipseMode(CENTER);
  cursorLayer.rectMode(CENTER);

  brushParameters = [lerpBrush, 'ellipse']; // won't run unless they're included in setup

}


// Start Program
function draw() {

  //Updateable background colour
  bgStack[0].background(paintPalette['0']);
  image(bgStack[0], 0, 0);
  
  // This is where the paintStack copies its overflow to
  image(bgStack[1], 0, 0);

  noStroke();
  cursor('assets/cursor-blank.png', 0, 0);

  // Painting functions
  activeBrush.apply(this, brushParameters);

  // Paint Stack Undo Stack and functions
  displayThepaintStack();

  // UI Layer and functions
  image(uiLayer, 0, uiPosition);
  showCurrentColour(); // selected colour preview box

  // Cursor Layer and functions
  image(cursorLayer, 0, 0);
  cursorLayer.clear();
  cursorController();
  
  continuousKeyControls(); //  must come after cursor() in order to be able to update them
  brushesAreOn = true; // some features (ie. colour picker) de-activate the brushes temporarily. this turns them back on.
}










/* Brush Mechanisms */

function activeBrush(brushName, shape) {
  if (mouseIsPressed && mouseY <= paintingAreaHeight-uiHeight && brushesAreOn && colourPickerOn==false) {
    brushName(shape, paintStack[undoCount]);
  } else if (mouseIsPressed && brushesAreOn && !uiIsVisible && brushesAreOn) {
    brushName(shape, paintStack[undoCount]);
  }
  updateMousePos();
}

function updateMousePos() { // prev mouse coordinates for the lerpBrush
  px = mouseX;
  py = mouseY;
}



/* Brush Collection */

function sloppyBrush(shape, layer) {
  brushRender(mouseX, mouseY, brushSize, shape, layer);
}
function orbitBrush(shape, layer) {
  let t = millis() / 50;
  let p = p5.Vector.fromAngle(t, brushSize*5).add(0, 0);
  // let col = paintColour;
  // col[3] = 255;
  // brushColour(paintStack[undoCount].fill(color(col)));
  brushRender(mouseX+p.x, mouseY+p.y, brushSize, shape, layer);
}

function sloppyDoubleBrush(shape, layer) {
  let easing1 = 0.1;
  let easing2 = 0.2;
  let easing3 = 0.3;
  let easing4 = 0.4;
  let easing5 = 0.5;
  let px1, px2, px3;
  let py1, py2, py3;
  px1 += (mouseX-px1)*easing1;
  py1 += (mouseY-py1)*easing1;
  px2 += (mouseX-px2)*easing2;
  py2 += (mouseY-py2)*easing2;
  px3 += (mouseX-px3)*easing3;
  py3 += (mouseY-py3)*easing3;

  brushRender(mouseX, mouseY, brushSize, shape, layer);
  // brushRender(px1, py1, brushSize, shape, layer);
  // brushRender(px2, py2, brushSize, shape, layer);
  // brushRender(mouseX*easing2, mouseY*easing5, brushSize, shape, layer);
  // brushRender(mouseX-20, mouseY+20, brushSize, shape, layer);
  // brushRender(mouseX-30, mouseY+30, brushSize, shape, layer);

}

function lerpBrush(shape, layer) { // lerp code to insert extra ellipses to smooth-out the brush
  d = dist(px, py, mouseX, mouseY);
  for (i = 1; i <= d; i += 1) {
    nx = lerp(px, mouseX, i / d);
    ny = lerp(py, mouseY, i / d);
    brushRender(nx,ny, brushSize, shape, layer);
  }
}





/* Brush Rendering */
function brushRender(nx, ny, brushSize, shape, layer) {
  brushColour();
                          brushShape(              nx,                ny, brushSize, shape, layer);
  // brushEffects();
  if (mirrorX)            brushShape(canvasWidth - nx,                ny, brushSize, shape, layer);
  if (mirrorY)            brushShape(              nx, canvasHeight - ny, brushSize, shape, layer);
  if (mirrorX && mirrorY) brushShape(canvasWidth - nx, canvasHeight - ny, brushSize, shape, layer);
  // if (radialMode)     ellipseBrush(canvasWidth - nx+100, canvasHeight - ny, brushSize);
}

function brushShape(posX, posY, size, shape, layer) {
  layer[shape](posX, posY, size);
}





/* Brush Colours */
function brushColour() {
  if (paintEraser){
    paintStack[undoCount].fill(color(paintPalette['eraser']));
  } else {
    paintStack[undoCount].fill(color(paintColour));
    rainbowMode();
  }
}
function rainbowMode() {
  if (activateRainbow == true) {
    paintColour = paintPalette.paintRainbow;
    paintCurrent = paintPalette.paintRainbow;
    paintEraser = false;
    loopTheIncreasingNumber(paintColour, 0, 255);
  } else { (fill(paintColour)); }
}






/* Brush Shapes */

function ellipseBrush(posX, posY, size) {
  ellipseMode(CENTER);
  ellipse(posX, posY, size, size)
}
function rectBrush(posX, posY, size) {
  rectMode(CENTER);
  rect(posX, posY, size*2, size);
}

function increaseBrushSize() {
  if (brushSize <= canvasWidth) {
    brushSize = plusOne(brushSize);
  }
}

function decreaseBrushSize() {
  if (brushSize > 1) {
    brushSize= minusOne(brushSize);
  } else if (brushSize = 1) {
    brushSize = 1;
  }
}



/* Various Tools */

// Brushes and Tools

function toggleEraserBrush() {
  paintEraser = !paintEraser;
  if (paintEraser == true) {eraserBrushOn();}
  if (paintEraser == false) {eraserBrushOff();}
}
function eraserBrushOn() {
  activateRainbow = false;
  updateEraserBrushColour();
  // paintColour = paintPalette['bgcolour'];
  logText('Erase On. Brush Colour: set to ' + paintColour);
}
function eraserBrushOff() {
  if (paintCurrent == paintPalette.paintRainbow) {
    activateRainbow = true;
    logText ('Rainbow Brush hm.')
  } else {
    paintColour = paintCurrent;
    logText('Erase Off wtf. Brush Colour: set to ' + paintColour);
  }
}
function updateEraserBrushColour() {
  paintPalette.eraser[0] = paintPalette[0][0];
  paintPalette.eraser[1] = paintPalette[0][1];
  paintPalette.eraser[2] = paintPalette[0][2];
}

/* Colour Droplet Picker */

function toolColourPicker() {
  brushesAreOn = false;
  colourPickerOn = true;
  // cursor('assets/cursor-dropper.png', 1, 23); // 24 x 24px graphic

  var cursorColourRGB = get(mouseX, mouseY);                // get RGBA array of pixel colour
  cursorColourRGB = 'rgb ' + cursorColourRGB;               // format the Array into a string for tinycolor

  var cursorColourHSB = tinycolor(cursorColourRGB).toHsv(); // use tinycolor.js to convert RGB to HSB

  var newColour = [];
  newColour[0] = map(cursorColourHSB.h, 0, 360, 0, 255);    // convert tinycolor's 0-360 range to 0-255
  newColour[1] = cursorColourHSB.s*255;                     // convert tinycolor's 0-1   range to 0-255
  newColour[2] = cursorColourHSB.v*255;                     // convert tinycolor's 0-1   range to 0-255
  
  palette.uiColourPreview(newColour[0],newColour[1],newColour[2],); // send HSB values to the colour-preview
  
  // update paintColour object with new colour values
  if (mouseIsPressed) {
    paintColour[0] = Math.round(newColour[0]);
    paintColour[1] = Math.round(newColour[1]);
    paintColour[2] = Math.round(newColour[2]);
  }
}








// Controls

function keyPressed() {
  if (key == 'z') {
    undo();
    console.log('Undo: ' +undoCount)
  }
  if (key == 'x') {
    redo();
  }
  if ( keymapSinglePress.hasOwnProperty(key)) { keymapSinglePress[key](); }
  if (key >= '0' && key <= '8') { colourKey(key); }                                   // Prefab Colours
  if (key == '9')                             { activateRainbow = true; logText('Rainbow Brush'); } // Rainbow Brush
  return false; // <-  defeats default browser behaviour
}


var  keymapSinglePress = { // single activation
  'Backspace':   clearCanvas,
  'p':   function () { hideCursor = true; setTimeout(saveImage, 50) },
  'h':   function () { mirrorX = !mirrorX },
  'v':   function () { mirrorY = !mirrorY },
  // 'f':   toggleEraserBrush,
  
  'i':   function () { brushParameters[0] = lerpBrush; },
  'o':   function () { brushParameters[0] = orbitBrush; },
  
  'k':   function () { brushParameters[1] = 'ellipse'; },
  'l':   function () { brushParameters[1] = 'rect';    },
  // 'j':   function () { brushParameters[1] = 'triangle'; },
  
  'Tab': function () { uiIsVisible = !uiIsVisible; uiVisibility(); },
  // 'u':  function () { hideCursor = !hideCursor; },
}

function colourKey(keyNumber) {
  paintColour = paintPalette[keyNumber];
  paintColourKeyNum = keyNumber;
  paintCurrent = paintColour;
  activateRainbow = false;
  paintEraser = false;
  logText('Colour: ' + paintColour)
}


function continuousKeyControls() {
  if (keyIsPressed) {
    if ( keymapContinuousPress.hasOwnProperty(key)) {
       keymapContinuousPress[key]();
      }
  }
}

var keymapContinuousPress = { // key must be held
  // 'u': activateBrush,
  'g'  : increaseBrushSize,
  'f'  : decreaseBrushSize,
  'w'  : function () { capTheIncreasingNumber  (paintColour, 2, 255) }, //Increase Value
  's'  : function () { capTheDecreasingNumber  (paintColour, 2, 0) },   //Decrease Value
  'd'  : function () { loopTheIncreasingNumber (paintColour, 0, 255) }, //Increase Hue
  'a'  : function () { loopTheDecreasingNumber (paintColour, 0, 0) },   //Decrease Hue
  'e'  : function () { capTheIncreasingNumber  (paintColour, 1, 255) }, //Increase Saturation
  'q'  : function () { capTheDecreasingNumber  (paintColour, 1, 0) },   //Decrease Saturation
  
  't'  : function () { //funcSomeArrays(capTheIncreasingNumber, paintPalette, 9, 3, 255);
                        if (paintEraser) {
                          console.log('eraser on');
                          capTheIncreasingNumber (paintPalette['eraser'], 3, 255 );
                          logText('Eraser Opacity: ' + paintPalette.eraser[3]);          //Increase Alpha
                        } else {
                          capTheIncreasingNumber (paintColour, 3, 255 ); 
                          logText('Opacity: ' + paintColour[3]);         } //Increase Alpha
                        },
  'r'  : function () { //funcSomeArrays(capTheDecreasingNumber, paintPalette, 9, 3, 0) 
                        if (paintEraser) {
                          console.log('eraser on');
                          capTheDecreasingNumber (paintPalette['eraser'], 3, 0 );
                          logText('Eraser Opacity: ' + paintPalette.eraser[3]);          //Increase Alpha
                        } else {
                          capTheDecreasingNumber (paintColour, 3, 0 ); 
                          logText('Opacity: ' + paintColour[3]);}         //Decrease Alpha
                        },
  'Alt': function () { toolColourPicker();}
}




// Other Functionality

function clearCanvas() {
  // if (confirm('Clear the canvas?')) {
    if (canvasClearCheck == false) {
    console.log('clearCanvas(): CCC ' + canvasClearCheck)

    // copy baseLayer to redoStack
    redoStack[0] = bgStack[1];
    
    // delete it
    bgStack.splice(1,1);
    
    // make a new baseLayer 
    bgStack[1] = createGraphics(canvasWidth, paintingAreaHeight)
    bgStack[1].colorMode(HSB, 255);
    bgStack[1].background(0,0,0,0);

    // copy paintStack to redoStack
    var copiedItem = paintStack[paintStack.length-1];
    for (i=1;i<paintStack.length;i++) {
      redoStack.splice(i,1,paintStack[i-1])
    }
    // clear paintStack
    paintStack.splice(0,paintStack.length);

    // add new item to paintStack
    newpaintStackItem(paintStack.length);
    undoCount = 0;
    canvasClearCheck = true;
    console.log('clearCanvas(): paintStack.length: ' + paintStack.length);
    }
  // }
}


function saveImage() {
  var date = year() + "-" + month() + "-" + day();
  var time = hour() + "h" + minute() + "m" + second() + "s";
  var fileName = 'symdraw' + "-" + "[" + date + "]-[" + time + "]";
  saveCanvas(fileName, 'png');
  console.log('Symdraw: Image Saved');
  hideCursor = false;
}














// UI Stuff

function cursorController() {
  if (brushSize<=5) {
    cursorStandard(5);
  }
  if (colourPickerOn) {
    cursor('assets/cursor-dropper.png', 1, 23); // 24 x 24px graphic
  } else {
    if (hideCursor) {
      cursor('assets/cursor-blank.png', 0, 0);
    } else {
      //draw the cursor
      if (uiIsVisible) {
        if (mouseY <= paintingAreaHeight - uiHeight) {
          cursorStandard(brushSize);
        } else {
          cursor(ARROW);
        }
      } else {
        cursorStandard(brushSize);
      }
      if (mouseY <= paintingAreaHeight - uiHeight && paintEraser) {
        cursor('assets/cursor-eraser.png', 1, 21);
      }
    }
  }
  colourPickerOn = false; // this has to be here for the controller to get the dropper cursor...
}

function cursorStandard(brushSize) {
  cursorLayer.stroke(50);
  brushRender(mouseX, mouseY, brushSize, brushParameters[1], cursorLayer);
  cursorLayer.stroke(255);
  brushRender(mouseX, mouseY, brushSize+2, brushParameters[1], cursorLayer);
}


function uiVisibility() {
  if (uiIsVisible) {
    uiPosition = paintingAreaHeight - consoleHeight;
  } else {
    uiPosition = -10000;
  }
}

class colourPalette {
  // width = paletteWidth;
  // height = paletteHeight-12;

  constructor(i) {
    this.x = ((paletteWidth*(i))+((i*4)*ui.marginH))-paletteWidth;
    this.y = ui.marginV+4;
    this.width = paletteWidth;
    this.height = paletteHeight-12;
  }
  display(i,y, o) {
    uiLayer.fill(paintPalette[i]);
    uiLayer.rect(this.x, y, this.width, o);
  }
  uiColourPreview(h, s, b) {
    uiLayer.fill(h, s, b);
    this.pos = ((paletteWidth*(paintColourKeyNum))+(((paintColourKeyNum*4))*ui.marginH))-paletteWidth;
    uiLayer.triangle(
      this.pos, this.y-3,
      this.pos, ui.bottom-2,
      this.pos+paletteWidth, ui.bottom-2,
    );
  }
}

function showCurrentColour() {
  showColourPalettesBG();
  showColourPalettes();
  showSelectedColourEmphasis();

}

function showColourPalettesBG() {
  uiLayer.fill(ui.bgColour);
  uiLayer.rect(0, 0, 555, uiHeight);
}
function showColourPalettes() {
  // for (p=1; p<9; p++) {
  uiLayer.fill(ui.bgColour)
  uiLayer.rect(((paletteWidth*(paintColourKeyNum))+((paintColourKeyNum*4)*ui.marginH))-paletteWidth, ui.marginV+1, paletteWidth, uiHeight);

  // paint the palettes 12 times, because the alpha lerp brush also overlaps itself a lot.
  // this way the palette alpha looks similar to the brush alpha
  for (j = 0; j< 9; j++) {
    for (i=1; i<9; i++) {
      palette = new colourPalette(i);
      palette.display(i, ui.marginV+4, paletteHeight-12);
    }
  }
}

function showSelectedColourEmphasis() {  
  uiLayer.fill(ui.bgColour)
  uiLayer.rect(((paletteWidth*(paintColourKeyNum))+((paintColourKeyNum*4)*ui.marginH))-paletteWidth, ui.marginV+1, paletteWidth, uiHeight);
    for (j = 0; j< 9; j++) {
      palettes = new colourPalette(paintColourKeyNum);
      palettes.display(paintColourKeyNum, ui.marginV+1, paletteHeight-7);
    }
}





// Undo Stuff

// Undo Stack Setup
function newpaintStackItem(i=paintStack.length) {
  paintStack.push(createGraphics(canvasWidth, paintingAreaHeight));
  paintStack[i].colorMode(HSB, 255);
  paintStack[i].rectMode(CENTER);
  paintStack[i].background(0,0);
  paintStack[i].noStroke();
}

function displayThepaintStack() {
  for (i = 0; i < paintStack.length; i++) {
    image(paintStack[i],0, 0);
  }
}
function displayTheRedoStack() {
  for (i = 0; i < redoStack.length; i++) {
    image(redoStack[i], 110+(i*110), 120);
  }
}

// Undo Stack Functionality
function undo() {
  if (canvasClearCheck) {
    console.log('Undo() CCC:' + canvasClearCheck)

    bgStack[1] = redoStack[0];
    redoStack.splice(0,1);

    for (i=0;i<redoStack.length;i++) {
      paintStack.splice(i,1,redoStack[i]);
    }
    redoStack.splice(0,redoStack.length);
    undoCount = paintStack.length;
    console.log('Undo(). paintStack: ' + paintStack.length + ' UndoCount: ' + undoCount)
    newpaintStackItem();
    canvasClearCheck = false;
  } else if (undoCount != 0) {
    copyItemToRedoStack();
    undoCount--;
    paintStack.splice(undoCount,1) // remove item from bottom of stack
  }
}

function undoCounter() {
  undoCount++;
  console.log('UndoCounter() undoCount: ' +undoCount)
  if (undoCount == undoMax) {
    copyLayer();
    paintStackShiftDown()
    undoCount = undoMax-1;
  }
  if (redoStack.length > 0) {
    redoStack.splice(0,redoStack.length)
    newpaintStackItem(paintStack.length);
  }
  if (paintStack.length < undoMax) {
    newpaintStackItem(paintStack.length);
  }
  if (canvasClearCheck == true) {
    canvasClearCheck = false;
    console.log('Click undoCounter() CCC:' + canvasClearCheck)
  }
}


function paintStackShiftDown() {
  paintStack.splice(0,1) // remove item from bottom of stack
  newpaintStackItem(paintStack.length);
}

function copyLayer() {
  bgStack[1].copy(paintStack[0], 0, 0, canvasWidth, paintingAreaHeight, 0, 0, canvasWidth, paintingAreaHeight);
}

// Redo Stack Functionality
function copyItemToRedoStack() {
  var copiedItem = paintStack[undoCount-1];
  redoStack.push(copiedItem);
  console.log('copyItemToRedoStack() redoStack.length: ' + redoStack.length);
}

function redo() {
  if (redoStack.length > 0 && canvasClearCheck == false ) {
    var copiedItem = redoStack[redoStack.length-1];
    paintStack.splice(undoCount, 0, copiedItem);
    redoStack.splice(redoStack.length-1, 1);
    undoCount++;
    console.log('Redo(): undoCount: ' +undoCount)
  }
}

function mouseClicked() {
  undoCounter();
  removeDOMcanvases();
}

// every time a createGraphics is run, it creates a DOM <canvas> element. they have to be removed manually
function removeDOMcanvases() {
  const canvases = document.querySelector(`[style="display: none; width: ${canvasWidth}px; height: ${canvasHeight}px;"]`);
  const body = document.querySelector('body')
  body.removeChild(canvases);
}










// Console Functionality

// Print to Logbox
function logText(consoleText) {
  drawLogBox();
  uiLayer.fill(ui['logText']);
  uiLayer.textSize(consoleTextSize);
  uiLayer.text(consoleText, (paletteWidth*8)+75, (consoleHeight/2)-(consoleTextSize/2), canvasWidth/2);
  setTimeout(logBoxClear , 2500);
}
// Draw the Logbox
function drawLogBox() {
  uiLayer.fill(ui['bgColour']);
  uiLayer.rect((paletteWidth*8)+70, 0, /*canvasWidth - paletteWidth - pickerPreviewWidth-ui.marginH*/300, canvasHeight);
}
// Clear the Logbox
function logBoxClear() {
  drawLogBox();
  // logBoxDecay();
}
// Fade the Logbox
function logBoxDecay() {
  alpha = 100;
  drawLogBox(color(255,0,255,alpha));
} 



// Functions to manipulate numbers
function plusOne(n)  {return n+1}
function plus05(n)   {return n+0.5}
function minusOne(n) {return n-1}

function capTheIncreasingNumber(arrayName, hsbSetting, numberCap) {
  if (arrayName[hsbSetting] >= numberCap) {
    arrayName[hsbSetting] = numberCap;
  } else {
    arrayName[hsbSetting] = plusOne(arrayName[hsbSetting]);
  }
}
function capTheDecreasingNumber(arrayName, hsbSetting, numberCap) {
  if (arrayName[hsbSetting] <= numberCap) {
    arrayName[hsbSetting] = numberCap;
  } else {
    arrayName[hsbSetting] = minusOne(arrayName[hsbSetting]);
  }
}
function funcSomeArrays(func, array, howManyArrays, position, numberCap) {
  for (i = 0; i < howManyArrays; i++) {
    func(array[i], position, numberCap);
  }
}

function loopTheIncreasingNumber(arrayName, hsbSetting, numberCap) {
    if (arrayName[hsbSetting] >= numberCap) {
      arrayName[hsbSetting] = 0;
    } else {
      arrayName[hsbSetting] = plusOne(arrayName[hsbSetting]);
    }
}
function loopTheDecreasingNumber(arrayName, hsbSetting, numberCap) {
    if (arrayName[hsbSetting] <= numberCap) {
      arrayName[hsbSetting] = 255;
    } else {
      arrayName[hsbSetting] = minusOne(arrayName[hsbSetting]);
    }
}



